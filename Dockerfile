FROM ruby:3.0.3

WORKDIR /myapp
COPY . .

RUN apt-get update -qq && apt-get install -y postgresql-client && bundle install

ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]