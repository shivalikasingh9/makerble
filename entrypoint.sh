#!/bin/bash
set -e

rm -f /myapp/tmp/pids/server.pid

bin/rails db:migrate

exec "$@"